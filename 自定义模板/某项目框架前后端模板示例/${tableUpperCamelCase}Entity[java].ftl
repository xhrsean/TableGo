<#-- 用于生成JPA实体JavaBean的自定义模板 -->
package ${jsonParam.packagePath}

<#if FtlUtils.fieldTypeExisted(tableInfo.fieldInfos, "Date")>
import java.util.Date;
import common.constant.GlobalConstant;
import com.fasterxml.jackson.annotation.JsonFormat;
</#if>
<#if FtlUtils.fieldTypeExisted(tableInfo.fieldInfos, "BigDecimal")>
import java.math.BigDecimal;
</#if>
import common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

<#assign importNotBlank = false />
<#assign importNotNull = false />
<#if tableInfo.fieldInfos?? && tableInfo.fieldInfos?size gt 0>
    <#list tableInfo.fieldInfos as fieldInfo>
    <#if !fieldInfo.primaryKey && fieldInfo.isNotNull>
        <#if !importNotBlank && fieldInfo.javaType == "String">
            <#assign importNotBlank = true />
import javax.validation.constraints.NotBlank;
        <#elseif !importNotNull && fieldInfo.javaType != "String">
            <#assign importNotNull = true />
import javax.validation.constraints.NotNull;
        </#if>
    </#if>
    </#list>
</#if>

/**
 * <#if StringUtils.isNotBlank(tableInfo.remark)>${tableInfo.remark}(${tableInfo.tableName})<#else>${tableInfo.tableName}</#if>
 * 
 * @author ${paramConfig.author}
 * @version 1.0.0 ${today}
 */
@ApiModel(description = "${tableInfo.simpleRemark!tableInfo.tableName}")
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "${tableInfo.tableName}")
public class ${tableInfo.upperCamelCase}Entity extends BaseBean {
    /** 版本号 */
    private static final long serialVersionUID = ${tableInfo.serialVersionUID!'1'}L;
<#if tableInfo.fieldInfos?? && tableInfo.fieldInfos?size gt 0>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkBegin, 1)} */
    </#if>
    <#list tableInfo.fieldInfos as fieldInfo>

    @ApiModelProperty(value = "${fieldInfo.remark}")
    <#if fieldInfo.primaryKey>
    @Id
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "uuid2")
    </#if>
    <#if !fieldInfo.primaryKey && fieldInfo.isNotNull>
        <#if fieldInfo.javaType == "String">
    @NotBlank(message = "${fieldInfo.simpleRemark!fieldInfo.proName}不能为空！")
        <#else>
    @NotNull(message = "${fieldInfo.simpleRemark!fieldInfo.proName}不能为空！")
        </#if>
    </#if>
    <#if fieldInfo.javaType == "Date">
    @JsonFormat(timezone = GlobalConstant.DEFAULT_TIMEZONE, pattern = GlobalConstant.DATE_TIME_PATTERN)
    </#if>
    private ${fieldInfo.javaType} ${fieldInfo.proName};
    </#list>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkEnd, 1)} */
    </#if>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkBegin, 2)} */
    </#if>
    <#list tableInfo.fieldInfos as fieldInfo>

    <#if paramConfig.buildFieldRemark == 0>
    /**
     * 获取${fieldInfo.remark!fieldInfo.proName}
     * 
     * @return ${fieldInfo.simpleRemark!fieldInfo.proName}
     */
    </#if>
    public ${fieldInfo.javaType} get${fieldInfo.upperCamelCase}() {
        return this.${fieldInfo.proName};
    }

    <#if paramConfig.buildFieldRemark == 0>
    /**
     * 设置${fieldInfo.remark!fieldInfo.proName}
     * 
     * @param ${fieldInfo.proName}
<#if fieldInfo.simpleRemark?? && fieldInfo.simpleRemark?trim != "">
     *          ${fieldInfo.simpleRemark}
</#if>
     */
    </#if>
    public void set${fieldInfo.upperCamelCase}(${fieldInfo.javaType} ${fieldInfo.proName}) {
        this.${fieldInfo.proName} = ${fieldInfo.proName};
    }
    </#list>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkEnd, 2)} */
    </#if>
</#if>
}