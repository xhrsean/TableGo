<#-- 初始化表的查询字段 -->
<#assign searchFeilds = FtlUtils.getJsonFieldList(jsonParam.searchFeilds, tableInfo.tableName) />
<#-- 判断是否是需要多表关联查询的表 -->
<#if FtlUtils.tableExisted(jsonParam.joinTables, tableInfo.tableName)>
    <#assign isJoinTable = true />
</#if>
package ${jsonParam.packagePath}

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

<#if !isJoinTable??>
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
</#if>
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
<#if FtlUtils.fieldTypeAtListExisted(searchFeilds, tableInfo.fieldInfos, "Date")>
import cn.hutool.core.date.DateUtil;
</#if>
import ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.model.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase};
import ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.model.condition.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}Condition;
import ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.mapper.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}Mapper;

/**
 * ${tableInfo.simpleRemark}Service接口实现
 *
 * @author ${paramConfig.author}
 * @version 1.0.0 ${today}
 */
@Service
@Transactional(readOnly = true)
public class ${tableInfo.upperCamelCase}Service extends ServiceImpl<${tableInfo.upperCamelCase}Mapper, ${tableInfo.upperCamelCase}> {
    private static final Logger logger = LoggerFactory.getLogger(${tableInfo.upperCamelCase}Service.class);

    /**
     * 根据查询条件分页查询${tableInfo.simpleRemark}列表
     *
     * @param condition 查询条件
     * @return 分页数据
     */
    public IPage<${tableInfo.upperCamelCase}> find${tableInfo.upperCamelCase}ByCondition(${tableInfo.upperCamelCase}Condition condition) {
        IPage<${tableInfo.upperCamelCase}> page = condition.buildPage();
<#assign fieldInfo = FtlUtils.getFieldByFieldTypeAtList(searchFeilds, tableInfo.fieldInfos, "Date") />
<#if isJoinTable?? && isJoinTable>
    <#if fieldInfo?has_content>

        if (condition.get${fieldInfo.upperCamelCase}End() != null) {
            condition.set${fieldInfo.upperCamelCase}End(DateUtil.endOfDay(condition.get${fieldInfo.upperCamelCase}End()));
        }
    </#if>
        return baseMapper.find${tableInfo.upperCamelCase}ByCondition(page, condition);
<#else>
        QueryWrapper<${tableInfo.upperCamelCase}> queryWrapper = condition.buildQueryWrapper(${tableInfo.upperCamelCase}.class);
    <#if fieldInfo?has_content>

        if (condition.get${fieldInfo.upperCamelCase}Begin() != null) {
            queryWrapper.lambda().ge(${tableInfo.upperCamelCase}::get${fieldInfo.upperCamelCase}, condition.get${fieldInfo.upperCamelCase}Begin());
        }
        if (condition.get${fieldInfo.upperCamelCase}End() != null) {
            queryWrapper.lambda().le(${tableInfo.upperCamelCase}::get${fieldInfo.upperCamelCase}, DateUtil.endOfDay(condition.get${fieldInfo.upperCamelCase}End()));
        }
    </#if>
        queryWrapper.lambda().orderByDesc(${tableInfo.upperCamelCase}::getCreationDate);

        return this.page(page, queryWrapper);
</#if>
    }

    /**
     * 根据主键ID查询${tableInfo.simpleRemark}信息
     *
     * @param ${tableInfo.pkLowerCamelName} 主键ID
     * @return ${tableInfo.simpleRemark}信息
     */
    public ${tableInfo.upperCamelCase} get${tableInfo.upperCamelCase}ById(${tableInfo.pkJavaType} ${tableInfo.pkLowerCamelName}) {
        return this.getById(${tableInfo.pkLowerCamelName});
    }

    /**
     * 新增${tableInfo.simpleRemark}信息
     *
     * @param ${tableInfo.lowerCamelCase} ${tableInfo.simpleRemark}信息
     * @return 是否成功
     */
    @Transactional
    public boolean add${tableInfo.upperCamelCase}(${tableInfo.upperCamelCase} ${tableInfo.lowerCamelCase}) {
        return this.save(${tableInfo.lowerCamelCase});
    }

    /**
     * 修改${tableInfo.simpleRemark}信息
     *
     * @param ${tableInfo.lowerCamelCase} ${tableInfo.simpleRemark}信息
     * @return 是否成功
     */
    @Transactional
    public boolean update${tableInfo.upperCamelCase}(${tableInfo.upperCamelCase} ${tableInfo.lowerCamelCase}) {
        return this.updateById(${tableInfo.lowerCamelCase});
    }

    /**
     * 根据主键ID删除${tableInfo.simpleRemark}
     *
     * @param ${tableInfo.pkLowerCamelName} 主键ID
     * @return 是否成功
     */
    @Transactional
    public boolean delete${tableInfo.upperCamelCase}ById(${tableInfo.pkJavaType} ${tableInfo.pkLowerCamelName}) {
        return this.removeById(${tableInfo.pkLowerCamelName});
    }

    /**
     * 根据主键ID列表批量删除${tableInfo.simpleRemark}
     *
     * @param idList 主键ID列表
     * @return 是否成功
     */
    @Transactional
    public boolean delete${tableInfo.upperCamelCase}ByIds(List<${tableInfo.pkJavaType}> idList) {
        return this.removeByIds(idList);
    }
}