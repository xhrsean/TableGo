<#-- 初始化表的模糊查询字段 -->
<#assign likeFeilds = FtlUtils.getJsonFieldList(jsonParam.likeFeilds, tableInfo.tableName) />
package ${jsonParam.packagePath}

<#if FtlUtils.fieldTypeExisted(tableInfo.fieldInfos, "Date")>
import java.util.Date;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
</#if>
<#if FtlUtils.fieldTypeExisted(tableInfo.fieldInfos, "BigDecimal")>
import java.math.BigDecimal;
</#if>
<#if FtlUtils.fieldAtListExisted(likeFeilds, tableInfo.fieldInfos)>
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.SqlCondition;
</#if>
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

<#assign importNotBlank = false />
<#assign importNotNull = false />
<#if tableInfo.fieldInfos?has_content>
    <#list tableInfo.fieldInfos as fieldInfo>
    <#if !fieldInfo.primaryKey && fieldInfo.isNotNull>
        <#if !importNotBlank && fieldInfo.javaType == "String">
            <#assign importNotBlank = true />
import javax.validation.constraints.NotBlank;
        <#elseif !importNotNull && fieldInfo.javaType != "String">
            <#assign importNotNull = true />
import javax.validation.constraints.NotNull;
        </#if>
    </#if>
    </#list>
</#if>
import ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.common.BaseBean;

/**
 * <#if StringUtils.isNotBlank(tableInfo.remark)>${tableInfo.remark}(${tableInfo.tableName})<#else>${tableInfo.tableName}</#if>
 *
 * @author ${paramConfig.author}
 * @version 1.0.0 ${today}
 */
@ApiModel(description = "${tableInfo.simpleRemark!tableInfo.tableName}")
@TableName("${tableInfo.tableName}")
public class ${tableInfo.upperCamelCase} extends BaseBean {
    /** 版本号 */
    private static final long serialVersionUID = ${tableInfo.serialVersionUID!'1'}L;
<#if tableInfo.fieldInfos?has_content>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkBegin, 1)} */
    </#if>
    <#list tableInfo.fieldInfos as fieldInfo>

    @ApiModelProperty(value = "${fieldInfo.remark}")
    <#if !fieldInfo.primaryKey && fieldInfo.isNotNull>
        <#if fieldInfo.javaType == "String">
    @NotBlank(message = "${fieldInfo.simpleRemark!fieldInfo.proName}不能为空！")
        <#else>
    @NotNull(message = "${fieldInfo.simpleRemark!fieldInfo.proName}不能为空！")
        </#if>
    </#if>
    <#if fieldInfo.primaryKey>
    @TableId
    <#elseif fieldInfo.javaType == "Date">
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    </#if>
    <#if FtlUtils.fieldExisted(likeFeilds, fieldInfo.colName)>
    @TableField(condition = SqlCondition.LIKE)
    </#if>
    private ${fieldInfo.javaType} ${fieldInfo.proName};
    </#list>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkEnd, 1)} */
    </#if>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkBegin, 2)} */
    </#if>
    <#list tableInfo.fieldInfos as fieldInfo>

    <#if paramConfig.buildFieldRemark == 0>
    /**
     * 获取${fieldInfo.remark!fieldInfo.proName}
     * 
     * @return ${fieldInfo.simpleRemark!fieldInfo.proName}
     */
    </#if>
    public ${fieldInfo.javaType} get${fieldInfo.upperCamelCase}() {
        return this.${fieldInfo.proName};
    }

    <#if paramConfig.buildFieldRemark == 0>
    /**
     * 设置${fieldInfo.remark!fieldInfo.proName}
     * 
     * @param ${fieldInfo.proName}
<#if StringUtils.isNotBlank(fieldInfo.simpleRemark)>
     *          ${fieldInfo.simpleRemark}
</#if>
     */
    </#if>
    public void set${fieldInfo.upperCamelCase}(${fieldInfo.javaType} ${fieldInfo.proName}) {
        this.${fieldInfo.proName} = ${fieldInfo.proName};
    }
    </#list>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkEnd, 2)} */
    </#if>
</#if>
}