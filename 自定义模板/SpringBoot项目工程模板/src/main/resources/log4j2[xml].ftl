<?xml version="1.0" encoding="UTF-8"?>

<Configuration status="WARN">
    <properties>
        <property name="LOG_HOME">/logs/${jsonParam.artifactId}</property>
        <property name="FILE_NAME">${jsonParam.artifactId}</property>
        <property name="LOGO_LEVEL">debug</property>
        <property name="PATTERN_LAYOUT">%d{yyyy-MM-dd HH:mm:ss.SSS}  %5p %r --- [%M:%L] %c : %m%n</property>
    </properties>
    
    <Appenders>
        <Console name="console" target="SYSTEM_OUT">
            <PatternLayout pattern="${"$"}{PATTERN_LAYOUT}"/>
        </Console>

        <RollingRandomAccessFile name="rollingRandomAccessFile" fileName="${"$"}{LOG_HOME}/${"$"}{FILE_NAME}.log" filePattern="${"$"}{LOG_HOME}/$${"$"}{date:yyyy-MM-dd}/${"$"}{FILE_NAME}_%d{yyyyMMdd}_%i.log.zip">
            <PatternLayout pattern="${"$"}{PATTERN_LAYOUT}"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
            <DefaultRolloverStrategy max="50"/>
        </RollingRandomAccessFile>
        
        <Async name="asyncAppender" includeLocation="true">
            <AppenderRef ref="rollingRandomAccessFile"/>
        </Async>
    </Appenders>

    <Loggers>
        <!-- 打印级别，大小写无关：TRACE, DEBUG, INFO, WARN, ERROR, ALL 和 OFF 默认是DEBUG -->
        <Root level="${"$"}{LOGO_LEVEL}">
            <AppenderRef ref="console"/>
            <AppenderRef ref="asyncAppender"/>
        </Root>

        <Logger name="org.springframework" level="error" additivity="true"/>
        <Logger name="net.sf.ehcache" level="error" additivity="true"/>
        <Logger name="freemarker.cache" level="error" additivity="true"/>
        <Logger name="freemarker.beans" level="error" additivity="true"/>
        <Logger name="org.quartz" level="error" additivity="true"/>
        <Logger name="org.apache.http" level="error" additivity="true"/>
        <Logger name="org.apache.commons.beanutils.converters" level="error" additivity="true"/>
        <Logger name="springfox.documentation" level="error" additivity="true"/>
        <Logger name="com.zaxxer.hikari.HikariConfig" level="error" additivity="true"/>
        <Logger name="com.baomidou.mybatisplus.core.MybatisConfiguration.addMappedStatement" level="error" additivity="true"/>
        <Logger name="com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize" level="error" additivity="true"/>
    </Loggers>
</Configuration>