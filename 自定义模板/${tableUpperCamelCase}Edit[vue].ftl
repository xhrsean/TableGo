<#-- 用于生成 Ant Design for Vue 的自定义模板 -->
<#if tableInfo.remark?has_content><!-- ${tableInfo.remark} --></#if>
<template>
  <div>
    <a-form @submit="handleSubmit" :form="form" class="form">
      <a-card class="card" title="${tableInfo.simpleRemark}" :bordered="false">
    <#list tableInfo.fieldInfos as fieldInfo>
        <#if fieldInfo_index % 2 == 0>
        <a-row class="form-row" :gutter="8">
        </#if>
        <#if fieldInfo.isDictType>
          <a-col :span="12">
            <a-form-item
              :labelCol="labelCol"
              :wrapperCol="wrapperCol"
              hasFeedback
              label="${fieldInfo.simpleRemark}"
            >
              <a-select
                  placeholder="请选择${fieldInfo.simpleRemark}"
                  v-decorator="['${fieldInfo.proName}',{rules: [{ required: false, message: '请选择${fieldInfo.simpleRemark}', whitespace: true}]}]">
                  <a-select-option v-for="p in ${fieldInfo.proName}s" :key="p.value">{{ p.name }}</a-select-option>
              </a-select>
            </a-form-item>
          </a-col>
        <#elseif fieldInfo.isMultiLineType>
          <a-col :span="12">
            <a-form-item
              :labelCol="labelCol"
              :wrapperCol="wrapperCol"
              hasFeedback
              label="${fieldInfo.simpleRemark}"
            >
             <a-textarea rows="3" placeholder="请输入${fieldInfo.simpleRemark}" v-decorator="['${fieldInfo.proName}', {rules: [{ required: false, message: '请输入${fieldInfo.simpleRemark}' }]}]" />
            </a-form-item>
          </a-col>
        <#else>
          <a-col :span="12">
            <a-form-item
              :labelCol="labelCol"
              :wrapperCol="wrapperCol"
              hasFeedback
              label="${fieldInfo.simpleRemark}"
            >
             <a-input
                v-decorator="[
                '${fieldInfo.proName}',
                {rules: [{ required: false, message: '${fieldInfo.simpleRemark}', whitespace: true}]}
              ]" />
            </a-form-item>
          </a-col>
        </#if>
        <#if (fieldInfo_index + 1) % 2 == 0 || !fieldInfo_has_next>
        </a-row>
            <#if fieldInfo_has_next>

            </#if>
        </#if>
    </#list>
      </a-card>
    </a-form>

    <!-- fixed footer toolbar -->
    <footer-tool-bar :style="{ width: isSideMenu() && isDesktop() ? 'calc(100% - ${"$"}{sidebarOpened ? 256 : 80}px)' : '100%'}">
      <a-button type="primary" @click="handleSubmit" :loading="loading">提交</a-button>
      <a-button style="margin-left: 8px" @click="handleGoBack">返回</a-button>
    </footer-tool-bar>
  </div>
</template>

<script>
import moment from 'moment'
import pick from 'lodash.pick'
import FooterToolBar from '@/components/FooterToolbar'
import { mixin, mixinDevice } from '@/utils/mixin'
import { listDictionaryByCategorys } from '@/api/basic/dictionary'
import { get${tableInfo.upperCamelCase}ById, modify${tableInfo.upperCamelCase} } from '@/api/basic/${tableInfo.lowerCamelCase}'
export default {
  name: '${tableInfo.upperCamelCase}Edit',
  mixins: [mixin, mixinDevice],
  props: {
    record: {
      type: [Object, String],
      default: ''
    }
  },
  components: {
    FooterToolBar
  },
  data () {
    return {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 5 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 }
      },
      buttonCol: {
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 12, offset: 5 }
        }
      },
      form: this.$form.createForm(this),
      loading: false,
<#list tableInfo.fieldInfos as fieldInfo>
    <#if fieldInfo.isDictType>
      ${fieldInfo.lowerCamelCase}s: []<#if fieldInfo_has_next>,</#if>
    <#else>
      ${fieldInfo.lowerCamelCase}: ''<#if fieldInfo_has_next>,</#if>
    </#if>
</#list>
    }
  },
  mounted () {
    this.$nextTick(() => {
      this.loadEditInfo(this.record)
    })
  },
  methods: {
    handleGoBack () {
      this.$emit('onGoBack')
    },
    handleSubmit () {
      const { form: { validateFields } } = this
      validateFields((err, values) => {
        if (!err) {
          this.loading = true
          const productJson = {
            ${tableInfo.pkLowerCamelName} : this.${tableInfo.pkLowerCamelName},
          }
          values = Object.assign(productJson, values)
          modify${tableInfo.upperCamelCase}(values).then(res => {
            this.$notification.success({
              message: '成功',
              description: '保存成功'
            })
            this.loading = false
            this.$emit('onGoBack')
          }).catch(err => {
            this.loading = false
            console.log('modify${tableInfo.upperCamelCase} catch ', err)
          })
        }
      })
    },
    loadEditInfo (data) {
      listDictionaryByCategorys(['','','']).then(res => {
<#list tableInfo.fieldInfos as fieldInfo>
    <#if fieldInfo.isDictType>
        this.${fieldInfo.lowerCamelCase}s = res.data['']
    </#if>
</#list>
      }).catch(err => {
        console.log('listDictionaryByCategorys catch ', err)
      })

      const { form } = this
      get${tableInfo.upperCamelCase}ById(data.${tableInfo.pkLowerCamelName}).then(res => {
        const formData = pick(res.data, [
            <#list tableInfo.fieldInfos as fieldInfo>'${fieldInfo.lowerCamelCase}'<#if fieldInfo_has_next>, </#if></#list>
        ])
        form.setFieldsValue(formData)
        this.${tableInfo.pkLowerCamelName}  = res.data.${tableInfo.pkLowerCamelName}
      }).catch(err => {
        console.log('get${tableInfo.upperCamelCase}ById catch ', err)
      })
    }
  }
}
</script>